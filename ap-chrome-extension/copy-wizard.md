# Copy Wizard
---

Copy wizard allows you to copy the assets from one system to another based on the configured settings.

Copying assets from source system to target involves the following steps:
<!-- TOC depthFrom:2 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Selecting the asset from exchange](#selecting-the-asset-from-exchange)
- [Kicking off copy wizard](#kicking-off-copy-wizard)
- [Configuring the source system](#configuring-the-source-system)
- [Configuring the Target System](#configuring-the-target-system)
	- [Configure Anypoint Platform as Target System](#configure-anypoint-platform-as-target-system)
- [Kick off the copy process](#kick-off-the-copy-process)

<!-- /TOC -->

## Selecting the asset from exchange
---
1. Navigate to exchange URL as usual.
2. If extension is installed correctly, you should now see Copy Assets button before the New button on the page and check boxes at the grid level below the assets as shown below. ![](_images/copy-wizard-53031cb5.png)
3. If you are in list view, you should see the Copy Assets button before the new button and checkboxes at the beginning of the select as shown below.![](_images/copy-wizard-278116ff.png)
4. Select/unselect the checkbox to select the asset to copy

> Note that even if you navigate away from the page and come back, the selected assets will still be selected - since the selection of assets is done at the browser level. You can remove the selected assets either by unselecting the asset from the UI screen or from the validate assets section of the copy wizard.

## Kicking off copy wizard
---
To kick off the asset copy feature, click the Copy Assets button which is available before the New button in the exchange window.![](_images/copy-wizard-278116ff.png)

If you have a valid Integralzone license to use the feature, you will be redirected to the copy asset wizard page as shown below.
![](_images/copy-wizard-68be6538.png)

> You need to have a valid Integralzone license to proceed to the next step. Please refer to the Licensing section to understand the process of requesting and applying license for the use of the plugin.

## Configuring the source system
---
During the copy operation, there are two types of systems possible to be configured - namely Source System and Target System. Depending on whether it is a source system or a target system and based on type of the system selected, the screen and its behavior differs slightly.

Let's take an example of the source system to see how the system can be configured.

1. Select the appropriate system from the list of options. Currently the options listed are Anypoint Exchange and Amazon S3 Bucket. ![](_images/copy-wizard-b8f2868c.png)

2. For the selected source system, configure the required system fields. For example, if you select Anypoint Exchange as the System Type, you would need to enter a valid Username and Password to validate with the system.
![](_images/copy-wizard-0c7eb6da.png)

3. When you click Next, the wizard will validate whether the provided properties for the system are valid.  ![](_images/copy-wizard-08544ba4.png)
> Note that the when you click Next button, the header section of Source changes to indicate the selected system type - for each reference during the copy process.

4. If you do not have license for a selected system type, appropriate error message will be displayed as shown below.
![](_images/copy-wizard-35aff841.png)

5. Once all the required fields are entered, you click on Next button to move to the next section.

## Configuring the Target System
---
Once source system has been configured, you will need to select the target system to copy the assets to. If you are selecting a system in Target side, the amount of information needed to be entered will be different compared to the source system - even though you selected similar system for source.

For example, for a target system of Anypoint Platform, you would need to select the target organization the asset needs to be copied to - as well as the user credentials to login to the system

### Configure Anypoint Platform as Target System
---
To configure Anypoint Platform as the target system, follow below steps:
1. Select System Type as Anypoint Exchange. Note that you should have options to enter Username, Password and select Organization. By default Organization will be blank.
2. Enter valid Username and Password for the Exchange.
3. When you lose focus on the Password field (or press tab from the password field), the username and password is validated against the platform. If the credentials are invalid, you will see an appropriate validation error message as shown below. ![](_images/copy-wizard-db601a05.png)
> Note: If the credentials are invalid, you will still see the Organization drop down to be empty and you won't be able to select the organization.

4. If the credentials are valid, then Organization field should be filled with valid list of Organizations the user has access to.
![](_images/copy-wizard-713fa344.png)

5. Once you are happy with the selection, click Next to proceed to the review screen

## Kick off the copy process
---
1. After configuring the target system, we should reach the review screen which lists all the selected assets from the source system.

2. If you would like to delete any asset from the list, select the trashcan icon next to the asset to delete the asset from the list. ![](_images/copy-wizard-a29ea71e.png)

3. You can navigate to configuring source/target by either clicking on the source or target train in the process flow or by using the Previous and Next buttons on the widget.

4. Once you are happy with the list of assets, click the Transfer Assets button to kick off the copy process. ![](_images/copy-wizard-7efa5603.png)
> Note: Initially all the asset will have the transfer status as "NOT YET STARTED". Once the process is kicked off, the status of the transfer changes.

5. Once the process is kicked off, the transfer status of the assets keep changing from phase to phase (like PREPARING, DOWNLOADING, PUBLISHING, etc) for each of the assets one by one. You should also see a Logs column for detailed debugging as needed.
![](_images/copy-wizard-f08a1a14.png)
> Note : The copy process doesn't copy assets in parallel. It copies the assets one by one in a sequential manner.

6. If you click the View button in the logs column of any asset, you can get detailed information regarding the transfer. ![](_images/copy-wizard-fec21934.png)

7. If the assets are successfully transferred or if the assets are not supported by the plugin yet, you can see the status like below. ![](_images/copy-wizard-b739a183.png)

8. In case of issue with the copy, relevant summary error will be displayed at the transfer step of the wizard as shown below. ![](_images/copy-wizard-f3a41b91.png)

9. If you look at the detailed logs of the transfer through the View option of the logs, you can get more detailed error messages for the transfer failure. ![](_images/copy-wizard-d5be9cc4.png)
