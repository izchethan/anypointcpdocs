# Extension Management
---

This document covers the following features:

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Installing the plugin](#installing-the-plugin)
- [Upgrading the plugin](#upgrading-the-plugin)
- [Uninstalling the plugin](#uninstalling-the-plugin)

<!-- /TOC -->

## Installing the plugin

--------------------------------------------------------------------------------

Follow these steps to install Anypoint Platform Chrome Plugin.

1. Make sure you have beta testing access to the plugin by sending an email to [chethan@integralzone.com](http://mailto:chethan@integralzone.com)
2. Once the access has been granted, visit the Chrome Web Store URL <https://chrome.google.com/webstore/detail/anypoint-platform/aibpipfodphlnkfjkkccoeoinijhmeho>
3. Once you see the chrome plugin option as shown below, please select the option "Add to Chrome" to install the plugin.![extension-management-16941909.png](/ap-chrome-extension/_images/extension-management-16941909.png)
4. You will get a pop up to add the plugin to the browser. Please select "Add extension" option to add to browser. ![](/ap-chrome-extension/_images/extension-management-66fb52cc.png)
5. Once successfully installed you should get an alert to inform successful installation like below.![](/ap-chrome-extension/_images/extension-management-3ca69341.png)
6. In case you want to enable the extension in incognito mode, go to [chrome://extensions](http://chrome://extensions) URL
7. Select the option "Allow in incognito" under the Anypoint Plugin to enable usage in incognito mode. ![](/ap-chrome-extension/_images/extension-management-a713c901.png)

## Upgrading the plugin

--------------------------------------------------------------------------------

1. To upgrade the plugin, go to chrome extensions page by going to [chrome://extensions](http://chrome://extensions) URL
2. Click the button "Update extensions now" on the top of the plugin page to update the plugin. ![](/ap-chrome-extension/_images/extension-management-00e00495.png)

## Uninstalling the plugin

--------------------------------------------------------------------------------

1. To uninstall the plugin, go to chrome extensions page by going to the [chrome://extensions](http://chrome://extensions) URL
2. Click the recycle bin icon to remove the anypoint platform plugin from chrome. ![](/ap-chrome-extension/_images/extension-management-272c6765.png)
3. Select the option "Remove" to confirm removal of the plugin from chrome.![](/ap-chrome-extension/_images/extension-management-17758ebb.png)
