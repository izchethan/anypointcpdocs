# Anypoint Platform Chrome Extension

Anypoint platform chrome extension aims at enhancing the capabilities of the Anypoint Platform by seamlessly adding value added features/utilities to the Anypoint Platform website. The intent of the extension is to provide extension to existing features of the platform to help with day to day operations on the platform.

Some of the key features of the platform provided as part of this release are:

- Copy of exchange assets from one anypoint platform account to another
- Support for backing up the exchange assets from anypoint platform to different target systems like Amazon S3 and Bitbucket Repository
- Support for restoring of the backed up assets from Amazon S3 and Bitbucket to a target Anypoint Platform

## See Also

- Learn more about [Installing and Managing the Extension](ap-chrome-extension/extension-management).
- Skip ahead and learn about [Copy Wizard](ap-chrome-extension/copy-wizard).
